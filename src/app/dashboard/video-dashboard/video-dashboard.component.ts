import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map, switchMap, filter, share } from 'rxjs/operators';

import { Video } from '../../app-types';
import { VideoDataService } from '../../video-data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent implements OnInit {
  selectedVideo: Observable<Video>;
  videoList: Observable<Video[]>;

  constructor(videoSvc: VideoDataService, route: ActivatedRoute) {
    this.videoList = videoSvc.loadVideos();

    this.selectedVideo = route.queryParams.pipe(
      filter(params => params['videoId']),
      map(params => params['videoId']),
      switchMap(id => videoSvc.loadSingleVideo(id)),
      share()
    );
  }

  ngOnInit() {
  }

}
