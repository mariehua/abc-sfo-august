import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Video } from './app-types';
import { map } from '../../node_modules/rxjs/operators';
import { Observable } from '../../node_modules/rxjs';

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {

  constructor(private http: HttpClient) { }

  loadVideos(): Observable<Video[]> {
    return this.http.get<Video[]>('https://api.angularbootcamp.com/videos');
  }

  loadSingleVideo(id: string): Observable<Video> {
    return this.http.get<Video>('https://api.angularbootcamp.com/videos/' + id);
  }
}
